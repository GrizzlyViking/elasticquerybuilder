<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:12
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Multi Match
    |--------------------------------------------------------------------------
    |
    | configure how the multimatch field is configured.
    |
    */
    'multiMatch' => [
        'type'   => [
            'best_fields'
        ],
        'fields' => [
            "title",
            "contributors",
            "series",
            "publisher"
        ]
    ],
];