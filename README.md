# Elastic Query Builder

## Installation
#### Composer
Require this package with composer using the following command:
```
    composer require wordery/search
```

#### Laravel
After updating composer, add the service provider to the `providers` array in `config/app.php`
```php
    \GrizzlyViking\QueryBuilder\SearchServiceProvider::class,
```

## Usage
### Fundamental ethos
The basic premise is that the query is built up as a tree. i.e. trunk, branches, leaves. Trunk is the class attribute `$this->body`,
the branches are `queries`, `filters`, `aggregations`, `sort`, `size` (pagination), `suggestions`. Some points worth mentioning is
that `scripts` and `filters` (the none post_filters) are attached to the `query` branch, and post_filters are
attached on the `aggregations` branch. Further note that post filters are split out onto trunk and aggregation filters.

#### Examples
##### Queries
```php
    $branch = Queries::create(
        MultiMatch::create('Harry Potter'),
        Query::create('must_not','terms', ['country' => ['GB']]),
        Query::create('must', ['Author'=> 'J. K. Rowlings']),
        Query::create('should', ['title' => 'Harry Potter'])
    );
```
##### Scripts
```php
    $string = "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))";
    $script = Scripts::create($string);
```
##### Filters
```php
    $branch = Filters::create(
        Filter::create('must_not', ['title' => 'Harry Potter'])->queryFilter(),
        Filter::create('must_not', ['author' => 'Rowlings']),
        Filter::create('should', ['contributor' => 'usborn'])
    );
```
##### Aggregations
```php
    $branch = Aggregations::create(
        [
            'title' => 'author',
            'field' => 'contributors.exact_matches_ci'
        ],
        Aggregation::create([
            'title' => 'interest Ages',
            'field' => 'interestAge',
            'order' => ['interestAge' => 'asc'],
            'ranges' => [
                ['key' => 'Baby', 'to' => 2],
                ['key' => 'Toddler', 'from' => 1, 'to' => 3],
                ['key' => '3-5 years', 'from' => 3, 'to' => 6],
                ['key' => '6-8 years', 'from' => 6, 'to' => 9],
                ['key' => '9-12 years', 'from' => 9, 'to' => 13],
                ['key' => '13+ years', 'from' => 13]
            ]
        ])
    );
```
##### Sort
```php
    Sort::create('bestsellerRanks', ['leadTime' => 'desc'])
```
##### Size
```php
    Size::create(10,12);
```


