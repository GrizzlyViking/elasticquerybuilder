<?php

namespace Tests\Unit;

use GrizzlyViking\QueryBuilder\Branches\Factories\Aggregations;
use GrizzlyViking\QueryBuilder\Branches\Factories\Queries;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Aggregation;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Filter;
use GrizzlyViking\QueryBuilder\Leaf\Factories\MultiMatch;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Query;
use GrizzlyViking\QueryBuilder\QueryBuilder;
use Tests\TestCase;

class QueryBuilderTest extends TestCase
{
    public function testSetFilters()
    {
        $builder = new QueryBuilder();

        $filters = [
            'series' => 'Harry Potter',
            'categories' => 'fantasy',
            'languages' => 'eng'
        ];
        foreach ($filters as $key => $filter) {

            $builder->setFilters(Filter::create('must',[$key => $filter]));
        }

        $expected = [
            "must" => [
                [
                    "match_phrase" => [
                        "series" => "Harry Potter"
                    ]
                ],
                [
                    "match" => [
                        "categories" => "fantasy"
                    ]
                ],
                [
                    "match" => [
                        "languages" => "eng"
                    ]
                ]
            ]
        ];

        $this->assertEquals(['bool' => $expected], $builder->getFilters()->build()->toArray());

        $builder->reset()->setFilters(
            Filter::create('must', ['title' => 'Harry Potter'])->postFilter(),
            Filter::create('must_not', ['author' => 'Rowlings'])->queryFilter(),
            Filter::create('should',   ['contributor' => 'usborn'])
        );

        $expected = [
            "must"     => [
                    "match_phrase" => [
                        "title" => "Harry Potter"
                    ]
            ],
            "must_not" => [
                    "match" => [
                        "author" => "Rowlings"
                    ]
            ],
            "should"   => [
                    "match" => [
                        "contributor" => "usborn"
                    ]
            ]
        ];

        $this->assertEquals(['bool' => $expected], $builder->getFilters()->build()->toArray());
    }

    /** @test */
    public function everything_and_the_kitchen_sink_query()
    {
        $query = (new QueryBuilder())->setQueries(
            Queries::create(
            MultiMatch::create('Harry Potter'),
            Query::create('must_not','terms', ['country' => ['GB']]),
            Query::create('must', ['Author'=> 'J. K. Rowlings']),
            Query::create('should', ['title' => 'Harry Potter'])
        ))
            ->setAggregates(Aggregations::create(
                Aggregation::create ([
                    'title' => 'author',
                    'field' => 'contributors.exact_matches_ci'
                ]),
                [
                    'title' => 'interest Ages',
                    'field' => 'interestAge',
                    'ranges' => [
                        ['key' => 'Baby', 'to' => 2],
                        ['key' => 'Toddler', 'from' => 1, 'to' => 3],
                        ['key' => '3-5 years', 'from' => 3, 'to' => 6],
                        ['key' => '6-8 years', 'from' => 6, 'to' => 9],
                        ['key' => '9-12 years', 'from' => 9, 'to' => 13],
                        ['key' => '13+ years', 'from' => 13]
                    ]
                ]
            ))
            ->setFilters(
                Filter::create('must', ['title' => 'Harry Potter'])->postFilter(),
                Filter::create('must_not', ['author' => 'Rowlings'])->queryFilter(),
                Filter::create('should',   ['contributor' => 'usborn'])
            )
            ->setScript("(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value" .
                " * (" .
                "0.25 * doc['scores.sales30ALL'].value + " .
                "0.1 * doc['scores.sales90ALL'].value + " .
                "0.005 * doc['scores.sales180ALL'].value + " .
                "0.05 * doc['scores.leadTime'].value + " .
                "0.15 * doc['scores.readyToGo'].value + " .
                "0.01 * doc['scores.hasJacket'].value + " .
                "0.01 * doc['scores.hasGallery'].value" .
                "))")
            ->setSort(['bestSellers' => 'desc'])
            ->setSource('isbn13', 'title')
            ->setSize(10,12)
            ->setSuggestions('Harry Putter', 'series')
        ;

        $expected = [
            "_source" => [
                "isbn13",
                "title"
            ],
            "query"   => [
                "function_score" => [
                    "query"=> [
                        "bool"=> [
                            "must" => [
                                [
                                    "multi_match"=> [
                                        "query" => "Harry Potter",
                                        "type" => "cross_fields",
                                        "operator" => "and",
                                        "fields" => [
                                            "title",
                                            "contributors",
                                            "series",
                                            "publisher"
                                        ]
                                    ]
                                ],
                                [
                                    "match_phrase"=> [
                                        "Author" => "J. K. Rowlings"
                                    ]
                                ]
                            ],
                            "must_not" => [
                                [
                                    "terms"=> [
                                        "country" => [
                                            "GB"
                                        ]
                                    ]
                                ]
                            ],
                            "should" => [
                                [
                                    "match_phrase"=> [
                                        "title" => "Harry Potter"
                                    ]
                                ]
                            ],
                            "filter"=> [
                                "bool"=> [
                                    "must_not"=> [
                                        "match"=> [
                                            "author" => "Rowlings"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "functions" => [
                        [
                            "script_score"=> [
                                "script"=> [
                                    "lang" => "painless",
                                    "inline" => "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))"
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "sort"    => [
                "bestSellers" => "desc"
            ],
            "size"    => 10,
            "from"    => 12,
            'suggest' => [
                'series' => [
                    'text' => 'Harry Putter',
                    'term' => [
                        'field' => 'series'
                    ]
                ],
            ],
            'aggregations' => [
                "author"        => [
                    'filter' => [
                        'bool' => [
                            'must' => ['match_phrase' => ['title' => 'Harry Potter']],
                            'should' => ['match'=> ['contributor' => 'usborn']]
                        ]
                    ],
                    'aggregations' => [
                        "author" => [
                            "terms" => [
                                "field" => "contributors.exact_matches_ci"
                            ]
                        ]
                    ]
                ],
                "interest Ages" => [
                    'filter' => [
                        'bool' => [
                            'must' => ['match_phrase' => ['title' => 'Harry Potter']],
                            'should' => ['match'=> ['contributor' => 'usborn']]
                        ]
                    ],
                    'aggregations' => [
                        "interest Ages" => [
                            "range" => [
                                "field"  => "interestAge",
                                "keyed"  => true,
                                "ranges" => [
                                    [
                                        "key" => "Baby",
                                        "to"  => 2
                                    ],
                                    [
                                        "key"  => "Toddler",
                                        "from" => 1,
                                        "to"   => 3
                                    ],
                                    [
                                        "key"  => "3-5 years",
                                        "from" => 3,
                                        "to"   => 6
                                    ],
                                    [
                                        "key"  => "6-8 years",
                                        "from" => 6,
                                        "to"   => 9
                                    ],
                                    [
                                        "key"  => "9-12 years",
                                        "from" => 9,
                                        "to"   => 13
                                    ],
                                    [
                                        "key"  => "13+ years",
                                        "from" => 13,
                                    ]
                                ]
                            ]
                        ],
                    ]
                ]
            ],
            'post_filter' => [
                'bool' => [
                    'must' => ['match_phrase' => ['title' => 'Harry Potter']],
                    'should' => ['match'=> ['contributor' => 'usborn']]
                ]
            ]
        ];


        $this->assertEquals($expected, $query->getQuery()->toArray());
    }

    /** @test */
    public function when_only_one_not_to_use_must()
    {
        $multiMatch = MultiMatch::create('Harry Potter');
        $multiMatch->setMultiMatchType(config('search.multiMatch.type'));
        $multiMatch->setFields(config('search.multiMatch.fields'));

        $query = (new QueryBuilder())->setQueries(Queries::create($multiMatch));

        $this->assertTrue(array_has($query->getQuery()->toArray(), 'query.multi_match'),
            'in the case where there is only one query item, then bool.must shouldn\'t be used');
    }

    /** @test */
    public function when_other_than_must_bool_used()
    {
        $query = (new QueryBuilder())->setQueries(
            Queries::create(
                Query::create('must_not', ['country' => ['GB']])
            ));

        $this->assertTrue(array_has($query->getQuery()->toArray(), 'query.bool.must_not'),
            'in the case where there is only one query item but when the boolean is anything else than must, then query.bool.must_not should be used.');
    }

    /** @test */
    public function when_multiple_queries_bool_operators_should_be_added()
    {
        $query = (new QueryBuilder())->setQueries(
            Queries::create(
                MultiMatch::create('Harry Potter'),
                Query::create('must_not',['country' => ['GB']]),
                Query::create('must', ['Author'=> 'J. K. Rowlings']),
                Query::create('should', ['title' => 'Harry Potter'])
            ));


        $this->assertTrue(array_has($query->getQuery()->toArray(), 'query.bool.must'),
            'in the case where there is multiple query items, then query.bool.must should be used');
    }

    /** @test  */
    public function test_that_sub_leaves_are_joined_with_should_and_leaf_stem_is_must()
    {

        $should_leaf = Query::create(['should' => [
            ['range' => ['interestAge' => ['gte' => 9, 'lte'=> 12]]],
            ['range' => ['interestAge' => ['gte' => 13]]]
        ]]);

        $query = (new QueryBuilder())->setQueries(
            Queries::create(
                Query::create('must', ['Author'=> 'J. K. Rowlings']),
                $should_leaf
            ));

        $this->assertContains($should_leaf->build(), array_get($query->getQuery()->toArray(), 'query'));
    }
}
