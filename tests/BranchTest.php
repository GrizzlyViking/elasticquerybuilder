<?php

namespace GrizzlyViking\QueryBuilder\tests;

use GrizzlyViking\QueryBuilder\Branches\Factories\Aggregations;
use GrizzlyViking\QueryBuilder\Branches\Factories\Filters;
use GrizzlyViking\QueryBuilder\Branches\Factories\Scripts;
use GrizzlyViking\QueryBuilder\Branches\Size;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Aggregation;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Filter;
use GrizzlyViking\QueryBuilder\Branches\Factories\Queries;
use GrizzlyViking\QueryBuilder\Branches\Factories\Sort;
use GrizzlyViking\QueryBuilder\Branches\Factories\Source;
use GrizzlyViking\QueryBuilder\Branches\Factories\Suggestions;
use GrizzlyViking\QueryBuilder\Leaf\Factories\MultiMatch;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Query;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Script;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Suggestion;
use GrizzlyViking\QueryBuilder\QueryBuilder;
use Tests\TestCase;

class BranchTest extends TestCase
{
    /** @test */
    public function attach_filters_to_aggregations()
    {
        $filters = Filters::create(
            Filter::create('must_not', ['title' => 'Harry Potter'])->postFilter(),
            Filter::create('must_not', ['publisher' => 'Daily Mail'])->postFilter(),
            Filter::create('must', ['contributors' => 'Rowlings'])->postFilter(),
            Filter::create('should', ['publisher' => 'usborn'])
        );

        /** @var \GrizzlyViking\QueryBuilder\Branches\Aggregations $aggregations */
        $aggregations = Aggregations::create(
            [
                'title' => 'author',
                'field' => 'contributors.exact_matches_ci'
            ],
            Aggregation::create([
                'title' => 'interest Ages',
                'field' => 'interestAge',
                'order' => ['interestAge' => 'asc'],
                'ranges' => [
                    ['key' => 'Baby', 'to' => 2],
                    ['key' => 'Toddler', 'from' => 1, 'to' => 3]
                ]
            ])
        )->setFilters($filters);

        $this->assertTrue(array_has($aggregations->build()->toArray(), 'author.filter.bool'),
            'in the case where there a filter has more than 1 element, and or there are more boolean values, then author.filter.bool should be used');

        $this->assertTrue(array_has($aggregations->build()->toArray(), 'author.filter.bool.should.match'),
            'in the case where there a filter has exactly one element, but the boolean value is not must, then \'author.filter.bool.should.match\' should be used');
    }

    /** @test */
    public function attaching_a_two_leaf_post_filter_should_not_appear_in_own_aggregation_leaf()
    {

        $filters = Filters::create(
            Filter::create(
                ['should' => [[
                    "term" => [
                        "contributors.exact_matches_ci" => "j. k. rowling"
                    ]
                ],
                [
                    "term" => [
                        "contributors.exact_matches_ci" => "j k rowling"
                    ]
                ]]])->postFilter()
        );

        $aggregations = Aggregations::create(
            [
                'title' => 'author',
                'field' => 'contributors.exact_matches_ci'
            ],
            Aggregation::create([
                'title' => 'interest Ages',
                'field' => 'interestAge',
                'order' => ['interestAge' => 'asc'],
                'ranges' => [
                    ['key' => 'Baby', 'to' => 2],
                    ['key' => 'Toddler', 'from' => 1, 'to' => 3]
                ]
            ])
        )->setFilters($filters);

        $this->assertFalse($aggregations->build()->get('author')->get('filter', false), 'The post filter for contributors shouldn\'t appear in aggregation of same name.');
    }

    /** @test */
    public function using_aggregations_with_filters()
    {
        $filters = Filters::create(
            Filter::create('must_not', ['title' => 'Harry Potter'])->queryFilter(),
            Filter::create('must_not', ['contributors' => 'Rowlings'])->postFilter(),
            Filter::create('should', ['publisher' => 'usborn'])
        );

        $aggregations = Aggregations::create(
            [
                'title' => 'author',
                'field' => 'contributors.exact_matches_ci'
            ],
            Aggregation::create([
                'title' => 'interest Ages',
                'field' => 'interestAge',
                'order' => ['interestAge' => 'asc'],
                'ranges' => [
                    ['key' => 'Baby', 'to' => 2],
                    ['key' => 'Toddler', 'from' => 1, 'to' => 3]
                ]
            ])
        )->setFilters($filters);

        $this->assertEquals([
            "author"        => [
                "filter"       => [
                    "bool" => [
                        "should" => [
                            "match" => [
                                "publisher" => "usborn"
                            ]
                        ]
                    ]
                ],
                "aggregations" => [
                    'author' => [
                        "terms" => [
                            "field" => "contributors.exact_matches_ci"
                        ]
                    ]
                ]
            ],
            "interest Ages" => [
                "filter" => [
                    "bool" => [
                        "must_not" => [
                            "match" => [
                                "contributors" => "Rowlings"
                            ]
                        ],
                        "should"   => [
                            "match" => [
                                "publisher" => "usborn"
                            ]
                        ]
                    ]
                ],
                "aggregations" => [
                    'interest Ages' => [
                        "range" => [
                            "field"  => "interestAge",
                            "keyed"  => true,
                            "order"  => [
                                "interestAge" => [
                                    "_count" => "asc"
                                ]
                            ],
                            "ranges" => [
                                [
                                    "key" => "Baby",
                                    "to"  => 2
                                ],
                                ["key" => "Toddler",
                                 "from" => 1,
                                 "to"   => 3
                                ]
                            ]
                        ]
                    ]
            ]
        ]], $aggregations->build()->toArray());
    }

    public function testAggregates()
    {
        $branch = Aggregations::create(
            [
                'title' => 'author',
                'field' => 'contributors.exact_matches_ci'
            ],
            Aggregation::create([
                'title' => 'interest Ages',
                'field' => 'interestAge',
                'order' => ['interestAge' => 'asc'],
                'ranges' => [
                    ['key' => 'Baby', 'to' => 2],
                    ['key' => 'Toddler', 'from' => 1, 'to' => 3],
                    ['key' => '3-5 years', 'from' => 3, 'to' => 6],
                    ['key' => '6-8 years', 'from' => 6, 'to' => 9],
                    ['key' => '9-12 years', 'from' => 9, 'to' => 13],
                    ['key' => '13+ years', 'from' => 13]
                ]
            ])
        );

        $this->assertEquals([
            "author"        => [
                "terms" => [
                    "field" => "contributors.exact_matches_ci"
                ]
            ],
            "interest Ages" => [
                "range" => [
                    "field"  => "interestAge",
                    "keyed"  => true,
                    "order"  => [
                        "interestAge" => [
                            "_count" => "asc"
                        ]
                    ],
                    "ranges" => [
                        [
                            "key" => "Baby",
                            "to"  => 2
                        ],
                        [
                            "key"  => "Toddler",
                            "from" => 1,
                            "to"   => 3
                        ],
                        [
                            "key"  => "3-5 years",
                            "from" => 3,
                            "to"   => 6
                        ],
                        [
                            "key"  => "6-8 years",
                            "from" => 6,
                            "to"   => 9
                        ],
                        [
                            "key"  => "9-12 years",
                            "from" => 9,
                            "to"   => 13
                        ],
                        [
                            "key"  => "13+ years",
                            "from" => 13,
                        ]
                    ]
                ]
            ]
        ], $branch->build()->toArray());
    }

    public function testFilters()
    {
        $branch_via_static = Filters::create(
            Filter::create('must_not', ['title' => 'Harry Potter'])->queryFilter(),
            Filter::create('must_not', ['author' => 'Rowlings']),
            Filter::create('should', ['contributor' => 'usborn'])
        );
        $this->assertTrue($branch_via_static->has('post_filter'));

        $branch = Filters::create(
            Filter::create('must_not', ['title' => 'Harry Potter'])->queryFilter(),
            Filter::create('must_not', ['author' => 'Rowlings']),
            Filter::create('should', ['contributor' => 'usborn'])
        );

        $this->assertEquals($branch_via_static, $branch);

        $this->assertEquals([
            'bool' => [
                'must_not' => [['match_phrase' => ['title' => 'Harry Potter']],['match' => ['author' => 'Rowlings']]],
                'should' => ['match'=> ['contributor' => 'usborn']]
            ]], $branch->build()->toArray(),
            'pure build returns'
        );

        $this->assertEquals([
            'bool' => [
                'must_not' => ['match' => ['author' => 'Rowlings']],
                'should' => ['match'=> ['contributor' => 'usborn']]
            ]], $branch->getPostFilters()->build()->toArray(),
            'post_filters only'
        );

        $this->assertEquals([
            'bool' => [
                'must_not' => ['match_phrase' => ['title' => 'Harry Potter']],
            ]], $branch->getQueryFilters()->build()->toArray(),
            'query filters only'
        );
    }

    public function testSort()
    {
        $this->assertEquals(
            ['bestsellerRanks' => 'asc', 'leadTime' => 'desc'],
            Sort::create('bestsellerRanks', ['leadTime' => 'desc'])->build()->toArray(), 'Sort Branch does not build as expected');
        $this->assertEquals(['bestsellerRanks' => 'asc'], Sort::create('bestsellerRanks')->build()->toArray(), 'Sort Branch does not build as expected');


        $this->assertTrue(Sort::create()->get()->isEmpty());
    }

    public function testSource()
    {
        $this->assertTrue(Source::create()->get()->isEmpty());

        $this->assertEquals(['bestsellerRanks', 'forSale'], Source::create('bestsellerRanks', 'forSale')->build()->toArray(), 'Source branch does not build as expected');
    }

    public function testSuggestions()
    {
        $this->assertEquals(['Series' => ["text" => "Harry Putter", "term" => [ "field" => "series" ]]],
            Suggestions::create('Harry Putter', 'series', 'Series')->build()->toArray());

        $branch = Suggestions::create(
            Suggestion::create('Harry Putter', 'series', 'Series'),
            Suggestion::create('Harry Puter', 'title', 'Title')
        );

        $expected = [
            'Series' => ["text" => "Harry Putter", "term" => [ "field" => "series" ]],
            'Title' => ["text" => "Harry Puter", "term" => [ "field" => "title" ]]
        ];

        $this->assertEquals($expected, $branch->build()->toArray());
    }

    public function testQueries()
    {
        $branch = Queries::create(
            MultiMatch::create('Harry Potter'),
            Query::create('must_not','terms', ['country' => ['GB']]),
            Query::create('must', ['Author'=> 'J. K. Rowlings']),
            Query::create('should', ['title' => 'Harry Potter'])
        );

        $expected = [
            'bool' => [
                "must"     => [
                    [
                        "multi_match" => [
                            "query"    => "Harry Potter",
                            "type"     => "cross_fields",
                            "operator" => "and",
                            "fields"   => [
                                "title",
                                "contributors",
                                "series",
                                "publisher",
                            ]
                        ]
                    ],
                    [
                        "match_phrase" => [
                            "Author" => "J. K. Rowlings",
                        ]
                    ]
                ],
                "must_not" => [
                    [
                        "terms" => [
                            "country" => [ "GB" ]
                        ]
                    ]
                ],
                "should"   => [
                    [
                        "match_phrase" => [ "title" => "Harry Potter" ]
                    ]
                ]
            ]
        ];

        $this->assertEquals($branch->getType(), 'query');

        $this->assertEquals($expected, $branch->build()->toArray(), 'Queries Branch does not build as expected');
    }

    /** @test */
    public function script_branch_builds_as_expected()
    {
        $branch = Scripts::create($parameters = [
            "script" => "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))",
            "score_mode" => "first",
            "boost_mode" => "replace",
        ]);

        $this->assertTrue(array_has($branch->build()->first(), ['lang', 'inline']));
    }

    /** @test */
    public function script_receives_a_string()
    {
        $string = "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))";
        $script = Scripts::create($string);

        $this->assertTrue($script->isNotEmpty());
        $this->assertNotTrue($script->hasScoreMode());
        $this->assertEquals($script->get()->first()->inline, $string);
    }

    /** @test */
    public function using_size_and_offset()
    {
        $branch = \GrizzlyViking\QueryBuilder\Branches\Factories\Size::create(10,12);

        $this->assertEquals(10, $branch->build()->get('size'));
        $this->assertEquals(12, $branch->build()->get('from'));
    }

    /** @test */
    public function build_query_branch_including_filters_and_scripts()
    {
        $string = "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))";
        $script = Scripts::create($string)->setBoostMode('replace')->setScoreMode('first');

        $branch = Queries::create(
            MultiMatch::create('Harry Potter'),
            Query::create('must_not','terms', ['country' => ['GB']]),
            Query::create('must', ['Author'=> 'J. K. Rowlings']),
            Filter::create('must_not',['term' => ['countryExclusion' => 'GB']])->queryFilter(),
            Filter::create('must', ['contributor' => 'Gary Larsson'])->queryFilter(),
            $script
        );

        $array = [];

        array_set($array, $branch->attachPoint(), $branch->build()->toArray());

        $this->assertEquals('query.function_score', $branch->attachPoint());
        $this->assertEquals(3, $branch->get()->count());

        $this->assertEquals(['match_phrase' => ['contributor' => 'Gary Larsson']], array_get($array, 'query.function_score.query.bool.filter.bool.must'));

        $functions = array_first(array_get($array, 'query.function_score.functions', []));
        $this->assertEquals($string, array_get($functions, 'script_score.script.inline'));
    }
}
