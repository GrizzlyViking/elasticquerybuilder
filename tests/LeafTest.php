<?php

namespace Tests\Unit;

use GrizzlyViking\QueryBuilder\Leaf\Factories\Aggregation;
use GrizzlyViking\QueryBuilder\Leaf\Factories\MultiMatch;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Query;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Script;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Suggestion;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Filter;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LeafTest extends TestCase
{
    /** @test */
    public function when_providing_very_little_it_guesses_type_and_boolean()
    {
        $leaf = Query::create(['title' => 'Harry Potter']);
        $this->assertEquals($leaf->getKey(), 'title', 'Key set incorrectly');
        $this->assertEquals($leaf->getBoolean(), 'must', 'Boolean set incorrectly. Boolean returned was '.$leaf->getBoolean());
        $this->assertEquals($leaf->getType(), 'match_phrase', 'Type set incorrectly. Boolean returned was '.$leaf->getType());
        $this->assertEquals($leaf->getValue(), 'Harry Potter', 'Value set incorrectly.');

        $this->assertEquals([ "match_phrase" => [ "title" => "Harry Potter" ]], $leaf->build());
    }

    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function confirm_that_leaf_is_built_correctly()
    {
        $leaf = Query::create('should', ['term' => ['title' => 'Harry Potter']]);
        $this->assertEquals([ "should" => [ "term" => [ "title" => "Harry Potter" ]]], [$leaf->getBoolean() => $leaf->build()]);

        $leaf = Query::create('match_all');
        $this->assertEquals(['match_all' => (object)[]], $leaf->build(), 'providing only a string, you get match_all');

        $leaf = Query::create(['term' => ['title' => 'Harry Potter']]);
        $this->assertEquals([ "term" => [ "title" => "Harry Potter" ]], $leaf->build());

        $leaf = Query::create('should', ['term' => ['title' => 'Harry Potter']]);
        $this->assertEquals([ "should" => [ "term" => [ "title" => "Harry Potter" ]]], [$leaf->getBoolean() => $leaf->build()]);
    }

    /** @test */
    public function throwing_odd_stuff_at_query_without_falling_over()
    {
        $leaf = Query::create('twink', ['term' => ['title' => ['!"Harry Potter"']]]);

        $this->assertEquals('title', $leaf->getKey());
        $this->assertEquals('term', $leaf->getType());
        $this->assertEquals('Harry Potter', $leaf->getValue());
        $this->assertEquals('must_not', $leaf->getBoolean());

        $this->assertEquals([ "must_not" => [ "term" => [ "title" => "Harry Potter"]]], [$leaf->getBoolean() => $leaf->build()], 'throwing weird stuff at query leaf, and should recover well.');
    }

    public function testFilterLeaf()
    {
        $this->assertEquals('filter', Filter::create(['term' => ['title' => 'Harry Potter']])->queryFilter()->getAttachPoint());

        $this->assertEquals('post_filter', Filter::create(['term' => ['title' => 'Harry Potter']])->postFilter()->getAttachPoint());
    }

    public function testAggregationLeaf()
    {
        $leaf = Aggregation::create([
            'title' => 'author',
            'field' => 'contributors.exact_matches_ci',
            'order'  => 'contributors'
        ]);
        $this->assertEquals(['terms' => ['field' => 'contributors.exact_matches_ci', 'order' => ['contributors' => ['_count' => 'desc']]]], $leaf->build()->toArray());


        $leaf = Aggregation::create([
            'title' => 'author',
            'field' => 'contributors.exact_matches_ci',
            'order'  => ['contributors' => 'asc'],
            'size'  => 10
        ]);
        $this->assertEquals(['terms' => ['field' => 'contributors.exact_matches_ci', 'size' => 10, 'order' => ['contributors' => ['_count' => 'asc']]]], $leaf->build()->toArray());

        $leaf = Aggregation::create([
            'title' => 'interest Ages',
            'field' => 'interestAge',
            'order' => ['interestAge' => 'desc'],
            'ranges' => [
                ['key' => 'Baby', 'to' => 2],
                ['key' => 'Toddler', 'from' => 1, 'to' => 3],
                ['key' => '3-5 years', 'from' => 3, 'to' => 6],
                ['key' => '6-8 years', 'from' => 6, 'to' => 9],
                ['key' => '9-12 years', 'from' => 9, 'to' => 13],
                ['key' => '13+ years', 'from' => 13]
            ]
        ]);

        $this->assertEquals([
            "range" => [
                "field"  => "interestAge",
                "keyed"  => true,
                'order'  => ['interestAge' => ['_count' => 'desc']],
                "ranges" => [
                    ['key' => 'Baby', 'to' => 2],
                    ['key' => 'Toddler', 'from' => 1, 'to' => 3],
                    ['key' => '3-5 years', 'from' => 3, 'to' => 6],
                    ['key' => '6-8 years', 'from' => 6, 'to' => 9],
                    ['key' => '9-12 years', 'from' => 9, 'to' => 13],
                    ['key' => '13+ years', 'from' => 13]
                ]

            ]
        ], $leaf->build()->toArray());
    }

    /** @test */
    public function add_shard_size_to_aggregation()
    {
        $leaf = Aggregation::create([
            'title' => 'author',
            'field' => 'contributors.exact_matches_ci',
            'sampler' => ['shard_size' => 10000]
        ]);


        $this->assertEquals(['sampler' => ['shard_size' => 10000], 'aggregations' => ['author' => ['terms' => ['field' => 'contributors.exact_matches_ci']]]], $leaf->build()->toArray());

    }

    public function testMultiMatchLeaf()
    {
        $multi = MultiMatch::create('Harry Potter');
        $expected = [
            "multi_match" => [
                "query"    => "Harry Potter",
                "type"     => "cross_fields",
                "operator" => "and",
                "fields"   => [
                    "title",
                    "contributors",
                    "series",
                    "publisher"
                ]
            ]
        ];

        $this->assertEquals($expected , $multi->build(), 'MultiMatch doesn\'t build as expected');
    }

    public function testScriptLeaf()
    {
        $script = Script::create("doc['bestsellerRanks.NBD'].value * (doc['forSale'].value == 0 ? 1000 : doc['forSale'].value < 3 ? 1 : doc['forSale'].value == 3 ? 3 : 1000)");

        $expected = [
            "lang"   => "painless",
            "inline" => "doc['bestsellerRanks.NBD'].value * (doc['forSale'].value == 0 ? 1000 : doc['forSale'].value < 3 ? 1 : doc['forSale'].value == 3 ? 3 : 1000)"
        ];

        $this->assertEquals($expected, $script->build(), 'Script leaf does not build as expected.');


        $script = Script::create($parameters = [
            "script" => "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))",
              "score_mode" => "first",
              "boost_mode" => "replace",
            ]);

        $this->assertEquals(['lang' => 'painless', 'inline'=>$parameters['script']], $script->build(), 'not building script as expected.');
    }

    public function testSuggestionLeaf()
    {
        $leaf = Suggestion::create()
            ->setSearchTerm('Harry Putter')
            ->setTitle('Series')
            ->setField('series');

        $this->assertEquals(['text' => 'Harry Putter', 'term' => ['field' => 'series']], $leaf->build());

        $leaf = Suggestion::create('Harry Putter', 'series', 'Series');
        $this->assertEquals(['text' => 'Harry Putter', 'term' => ['field' => 'series']], $leaf->build());
        $this->assertEquals('Series', $leaf->getTitle());
    }

    /** @test */
    public function build_filter_that_uses_ranges()
    {

        $leaf = Filter::create([
            "range" => [
                "interestAge" => [
                    "gte" => "9",
                    "lt"  => "12"
                ]
            ]
        ]);

        $this->assertEquals('range', $leaf->getType(), 'Range was not assigned to type.');
        $this->assertEquals('interestAge', $leaf->getKey(), 'Key was not assigned.');

        $this->assertEquals([
            "range" => [
                "interestAge" => [
                    "gte" => "9",
                    "lt"  => "12"
                ]
            ]
        ], $leaf->build());
    }

    /** @test */
    public function if_boolean_is_provided_as_first_argument_and_there_are_more_than_one_value_then_branch_out()
    {
        $leaf = Query::create(['should' => [
            ["range" => [
                "interestAge" => [
                    "gte" => "9",
                    "lt"  => "12"
                ]
            ]],
            ["range" => [
                "interestAge" => [
                    "gte" => "13",
                ]
            ]]
        ]]);

        $this->assertEquals([
            "bool" => [
                "should" => [
                    [
                        "range" => [
                            "interestAge" => [
                                "gte" => "9",
                                "lt"  => "12"
                            ]
                        ]
                    ],
                    [
                        "range" => [
                            "interestAge" => [
                                "gte" => "13"
                            ]
                        ]
                    ]
                ]
            ]
        ], $leaf->build());
    }

    /** @test */
    public function even_if_boolean_is_provided_but_its_only_one_leaf_then_leaf_is_flattened_to_its_most_concise_format()
    {
        $leaf = Filter::create(['should' => [
            ["range" => [
                "interestAge" => [
                    "gte" => "9",
                    "lt"  => "12"
                ]
            ]]
        ]]);

        $this->assertEquals([
            "range" => [
                "interestAge" => [
                    "gte" => "9",
                    "lt"  => "12"
                ]
            ]
        ], $leaf->build());
    }

    /** @test */
    public function drilling_down_threw_an_array_allocates_correctly()
    {
        $leaf = Filter::create(['must_not' => ['terms' => ['salesExclusions' => ['GB']]]]);

        $this->assertEquals($leaf->getBoolean(), 'must_not');
        $this->assertEquals($leaf->getType(), 'terms');
        $this->assertEquals($leaf->getKey(), 'salesExclusions');
        $this->assertEquals($leaf->getValue(), ['GB']);
    }

    /** @test */
    public function filters_provided_as_array_to_build_correctly()
    {

        $leaf = Filter::create(['contributors'   => ['j. k. rowling']]);

        $this->assertEquals('must', $leaf->getBoolean(), 'must was not assigned to boolean, "'.$leaf->getBoolean().'" was returned.');
        $this->assertEquals('match_phrase', $leaf->getType(), 'incorrect query type guessed.');
        $this->assertEquals('post_filter', $leaf->getAttachPoint());
    }

    /** @test */
    public function filters_use_must_not()
    {
        $leaf = Filter::create('must_not', ['title' => 'Harry Potter'])->postFilter();

        $this->assertEquals('must_not', $leaf->getBoolean(), 'must_not was not assigned to boolean, "'.$leaf->getBoolean().'" was returned.');
        $this->assertEquals('title', $leaf->getKey());
        $this->assertEquals('post_filter', $leaf->getAttachPoint());
    }

    /** @test */
    public function setting_filter_type()
    {
        $leaf = Filter::create('must_not','terms', ['country' => ['GB']]);

        $this->assertEquals('must_not', $leaf->getBoolean(), 'must_not was not assigned to boolean, "'.$leaf->getBoolean().'" was returned.');
        $this->assertEquals('country', $leaf->getKey());
        $this->assertEquals('terms', $leaf->getType());
        $this->assertEquals(['GB'], $leaf->getValue());
        $this->assertEquals('post_filter', $leaf->getAttachPoint());
    }

    /** @test */
    public function send_a_string_to_set_scripts()
    {
        $string = "(1 + Math.pow(_score, 0.5) * doc['scores.inStock'].value * (0.25 * doc['scores.sales30ALL'].value + 0.1 * doc['scores.sales90ALL'].value + 0.005 * doc['scores.sales180ALL'].value + 0.05 * doc['scores.leadTime'].value + 0.15 * doc['scores.readyToGo'].value + 0.01 * doc['scores.hasJacket'].value + 0.01 * doc['scores.hasGallery'].value))";
        $script = Script::create($string);

        $this->assertEquals($script->lang, 'painless');
        $this->assertEquals($script->inline, $string);
    }

    /** @test */
    public function another_way_of_building_a_filter()
    {
        $filter = Filter::create('should', 'terms', ['country' => ['GB']]);

        $this->assertEquals( 'should', $filter->getBoolean());
        $this->assertEquals( 'country', $filter->getKey());
        $this->assertEquals( 'terms', $filter->getType());
        $this->assertEquals( ['GB'], $filter->getValue());
    }
}
