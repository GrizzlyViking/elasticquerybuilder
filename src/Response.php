<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 30/11/2017
 * Time: 17:13
 */

namespace GrizzlyViking\QueryBuilder;


use Illuminate\Support\Collection;

class Response implements ResponseInterface
{
    /** @var \Illuminate\Support\Collection */
    protected $books;
    protected $resultMetaData;
    protected $aggregations;

    public function __construct($elasticResponse)
    {
        $this->resultMetaData = collect(array_except($elasticResponse, ['hits.hits', 'aggregations']));

        if (array_has($elasticResponse, 'hits.total') && array_has($elasticResponse, 'hits.hits')) {
            $this->books = collect(array_get($elasticResponse, 'hits.hits'));
        } else {
            $this->books = collect();
        }

        if ($aggregations = array_get($elasticResponse, 'aggregations', false)) {

            $aggregations_config = collect(config('search.aggregations'))->pluck('title');
            $this->aggregations = collect($aggregations)->sortBy(function($agg, $key) use ($aggregations_config) {
                return $aggregations_config->search($key);
            });
        }
    }

    public function all()
    {
        return [
            'books' => $this->books,
            'facets' => $this->getAggregations()
        ];
    }

    /**
     * @return Collection
     */
    public function getIsbns()
    {
        return $this->books->pluck('_id');
    }

    /**
     * @return Collection
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @param string $key
     * @param array $value
     */
    public function setAggregation($key, $value)
    {
        $this->aggregations->put($key, $value);
    }

    /**
     * @return Collection
     */
    public function getResultMetaData(): Collection
    {
        return $this->resultMetaData;
    }
}