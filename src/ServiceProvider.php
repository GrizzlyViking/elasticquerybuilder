<?php

namespace GrizzlyViking\QueryBuilder;

use Elasticsearch\ClientBuilder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/queryBuilder.php' => config_path('queryBuilder.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/queryBuilder.php', 'queryBuilder'
        );

        $this->app->singleton('ElasticSearch', function(){

            $host = [
                env('ELASTICSEARCH_HOST', 'localhost').':'.env('ELASTICSEARCH_PORT', 9200)
            ];

            return ClientBuilder::create()->setHosts($host)->build();
        });
    }
}
