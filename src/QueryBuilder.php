<?php

namespace GrizzlyViking\QueryBuilder;

use Elasticsearch\Client;
use GrizzlyViking\QueryBuilder\Branches\Aggregations;
use GrizzlyViking\QueryBuilder\Branches\BranchInterface;
use GrizzlyViking\QueryBuilder\Branches\Filters;
use GrizzlyViking\QueryBuilder\Branches\Queries;
use GrizzlyViking\QueryBuilder\Branches\Scripts;
use GrizzlyViking\QueryBuilder\Branches\Size;
use GrizzlyViking\QueryBuilder\Branches\Sort;
use GrizzlyViking\QueryBuilder\Branches\Source;
use GrizzlyViking\QueryBuilder\Branches\Suggestions;
use GrizzlyViking\QueryBuilder\Branches\Factories\Suggestions as SuggestionsFactory;
use GrizzlyViking\QueryBuilder\Leaf\Filter;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Filter as FilterFactory;
use GrizzlyViking\QueryBuilder\Leaf\Query;
use GrizzlyViking\QueryBuilder\Leaf\Script;
use Illuminate\Support\Collection;

class QueryBuilder implements QueryBuilderInterface {

    use BespokeValidationTrait;

    /** @var  Aggregations */
    protected $_aggregations;
    /** @var  Collection */
    protected $_body;
    /** @var  Filters */
    protected $_filters;
    /** @var  Scripts */
    protected $_script;
    /** @var  Size */
    protected $_size;
    /** @var  Sort */
    protected $_sort;
    /** @var  Suggestions */
    protected $_suggestions;
    /** @var  Queries */
    protected $_queries;
    /** @var  Source */
    protected $_source;

    public function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->_body = collect();
        $this->_filters = new Filters();
        $this->_script = new Scripts();
        $this->_size = new Size();
        $this->_sort = new Sort();
        $this->_source = new Source();
        $this->_suggestions = new Suggestions();
        $this->_aggregations = new Aggregations();
        $this->_queries = new Queries();

        return $this;
    }
    
    /**
     * @return $this
     */
    public function build()
    {
        if ($this->hasBeenBuilt()) {
            $this->_body = collect();
        }

        if ($this->_source->isNotEmpty()) {
            $this->set($this->_source->attachPoint(), $this->_source->build()->toArray());
        }

        if ($this->getFilters()->has('filter')) {
            $this->_queries->setFilters($this->getFilters()->getQueryFilters());
        }

        if ($this->_script->isNotEmpty()) {
            $this->_queries->setScripts($this->_script);
        }

        $this->set($this->_queries->attachPoint(), $this->_queries->build()->toArray());

        if ($this->_sort->isNotEmpty()) {
            $this->set($this->_sort->attachPoint(), $this->_sort->build()->toArray());
        }

        if ($this->_size->isNotEmpty()) {
            $this->_size->build()->each(function($value, $key) {
                $this->_body->put($key, $value);
            });
        }

        if ($this->_suggestions->isNotEmpty()) {
            $this->set($this->_suggestions->attachPoint(), $this->_suggestions->build()->toArray());
        }


        if ($this->getFilters()->has('post_filter')) {
            $this->set('post_filter', $this->getFilters()->getPostFilters()->build()->toArray());
        }

        if ($this->_aggregations->isNotEmpty()) {
            if ($this->getFilters()->has('post_filter')) {
                $this->_aggregations->setFilters($this->getFilters()->getPostFilters());
            }
            $this->set($this->_aggregations->attachPoint(), $this->_aggregations->build()->toArray());
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getQuery()
    {
        if (!$this->hasBeenBuilt()) {

            $this->build();
        }
        return $this->_body;
    }

    /**
     * @param BranchInterface $queries
     * @return $this
     */
    public function setQueries($queries)
    {
        $queries->get()->each(function($leaf) {
            $this->_queries->add($leaf);
        });

        return $this;
    }

    /**
     * @param string $path
     * @param mixed $value
     * @return $this
     */
    public function set($path, $value)
    {
        $body = $value;
        foreach (array_reverse(explode('.', $path)) as $item) {
            $body = [$item => $body];
        }

        $this->mergeRecursive($this->_body, $body);

        return $this;
    }

    /**
     * @param Collection $attribute
     * @param mixed $value
     * @return $this
     */
    protected function mergeRecursive(&$attribute, $value)
    {
        if (!is_null($value) && !empty($value)) {
            $attribute = collect(array_merge_recursive($attribute->toArray(), $value));
        }

        return $this;
    }

    /**
     * @return Aggregations
     */
    public function getAggregates()
    {
        return $this->_aggregations;
    }

    /**
     * @param Aggregations $aggregates
     * @return $this
     */
    public function setAggregates($aggregates)
    {
        $this->_aggregations = $aggregates;
        return $this;
    }

    /**
     * @param Filters|Filter $filters
     * @return $this
     */
    public function setFilters(...$filters)
    {
        if (count($filters) == 1 && reset($filters) instanceof Filters) {
            $this->_filters = reset($filters);

            return $this;
        }

        foreach ($filters as $key => $filter) {
            if ($filter instanceof Filter) {
                $this->_filters->add($filter);
            } else {
                $this->_filters->add(FilterFactory::create([$key => $filter]));
            }
        }

        return $this;
    }

    /**
     * @return Filters
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @param mixed $script
     * @return $this
     */
    public function setScript($script)
    {
        $this->_script = \GrizzlyViking\QueryBuilder\Branches\Factories\Scripts::create($script);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getScripts()
    {
        return $this->_script;
    }

    public function debug()
    {
        return 'debug';
    }

    /**
     * @param mixed $sort
     * @return $this
     */
    public function setSort($sort)
    {

        if (! $sort instanceof Sort) {

            $sort = \GrizzlyViking\QueryBuilder\Branches\Factories\Sort::create($sort);
        }

        $this->_sort = $sort;

        return $this;
    }

    /**
     * @param array ...$source
     * @return $this
     */
    public function setSource(...$source)
    {
        if (! $source instanceof Source) {
            $source = (new Source())->set($source);
        }

        $this->_source = $source;

        return $this;
    }

    /**
     * @param array ...$arguments
     * @return $this
     */
    public function setSize(...$arguments)
    {
        if ($first = collect($arguments)->first() instanceof Size) {
            $this->_size = $first;
        } else {

            $this->_size = (new Size())->set($arguments);
        }

        return $this;
    }

    public function getPagination()
    {
        return $this->_size;
    }

    /**
     * @param mixed $suggestions
     * @return $this
     */
    public function setSuggestions($searchTerm, $field, $title = null)
    {

        $this->_suggestions = SuggestionsFactory::create($searchTerm, $field, $title);

        return $this;
    }

    /**
     * @param array $index
     * @return Response
     */
    public function search(array $index = []): Response
    {
        $query = array_merge(
            [
                'index' => config('search.index.index', 'books'),
                'type'  => config('search.index.type', 'book'),
                'body'  => $this->getQuery()
            ], $index);

        return new Response(app('ElasticSearch')->search($query));
    }

    private function hasBeenBuilt()
    {
        return ($this->_body instanceof Collection && $this->_body->isNotEmpty());
    }
}