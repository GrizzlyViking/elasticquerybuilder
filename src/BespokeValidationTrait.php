<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:26
 */

namespace GrizzlyViking\QueryBuilder;


trait BespokeValidationTrait
{
    function areTheyAll($array, $callback = 'is_string')
    {
        $passed = true;

        if ( ! $array instanceof Collection) {
            $array = collect($array);
        }

        $array->each(function ($element) use (&$passed, $callback) {
            $passed = $callback($element);

            if ($passed === false) return false;
            return true;
        });

        return $passed;
    }
}