<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 08/06/2017
 * Time: 12:55
 */

namespace GrizzlyViking\QueryBuilder\Branches;


use GrizzlyViking\QueryBuilder\Leaf\Filter;
use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\Leaf\MultiMatch;
use GrizzlyViking\QueryBuilder\Leaf\Query;
use GrizzlyViking\QueryBuilder\Leaf\Script;
use GrizzlyViking\QueryBuilder\ManageBranchTrait;
use Illuminate\Support\Collection;

class Queries implements BranchInterface
{
    use ManageBranchTrait;

    /**
     * @var Collection
     */
    protected $leaves;
    /** @var  Filters */
    protected $_queryFilters;
    /** @var Scripts */
    protected $scripts;
    /** @var string */
    protected $attachPoint = 'query';

    /**
     * Queries constructor.
     * @param mixed ...$arguments
     */
    public function __construct(...$arguments)
    {
        $this->leaves = collect();

        foreach ($arguments as $argument) {
            switch (true) {
                case $argument instanceof Filter && $argument->getAttachPoint() == 'filter':
                    $this->addFilter($argument);
                    break;
                case $argument instanceof Script || $argument instanceof Scripts:
                    $this->setScripts($argument);
                    break;
                case $argument instanceof LeafInterface:
                    $this->add($argument);
                    break;
            }
        }
    }

    function get(): Collection
    {
        return $this->leaves;
    }

    function getType(): string
    {
        return 'query';
    }

    /**
     * @return bool
     */
    private function shouldUseBool()
    {
        return (
            $this->get()->count() > 1 ||
            ($this->get()->first() instanceof LeafInterface && $this->get()->first()->getBoolean() !== 'must') ||
            ($this->_queryFilters instanceof Filters && $this->_queryFilters->isNotEmpty())
        );
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {
        $query = $this->get()->when($this->shouldUseBool(),
            function ($collection) {
                return $collection->groupBy(function ($leaf) {
                    /** @var Query|MultiMatch $leaf */
                    return $leaf->getBoolean();
                });
            })->map(function ($leaves) {

            if ($leaves instanceof Collection) {
                return $leaves->map(function ($leaf) {
                    /** @var Query|MultiMatch $leaf */
                    return $leaf->build();
                })->toArray();
            }

            /** @var Query|MultiMatch $leaves */
            return $leaves->build();
        });

        if ($query->count() == 1 && !in_array($query->keys()->first(), ['should', 'must', 'must_not'], true)) {
            $query = collect($query->first());
        }

        if ($this->hasFilters()) {
            $query->put('filter', $this->_queryFilters->build()->toArray());
        }

        if ($this->getScripts() instanceof Scripts && $this->getScripts()->isNotEmpty()) {

            $array = collect();
            $array->put('query', ['bool' => $query->toArray()]);
            $array->put('functions', [['script_score' => ['script' => $this->getScripts()->build()->first()]]]);

            if ($this->getScripts()->hasScoreMode()) {
                if ($this->getScripts()->getScoreMode()) {
                    $array->put('score_mode', $this->getScripts()->getScoreMode());
                }
                if ($this->getScripts()->getBoostMode()) {
                    $array->put('boost_mode', $this->getScripts()->getBoostMode());
                }
            }

            $query = $array;

            if($query->get('query') && count($query->get('query')) > 1) {
                $query->put('query', ['bool' => $query->get('query')]);
            }
        }

        if ($query->count() > 1 && $query->keys()->diff(['should', 'must_not', 'filter', 'must'])->isEmpty()) {
            // if there is more than 1 item, and they are all boolean values, then add boolean in front.
            $query = collect(['bool' => $query->toArray()]);
        } elseif($query->count() == 1 && $query->keys()->diff(['should', 'must_not', 'filter'])->isEmpty()) {
            // so if there is one root, and only if its boolean, except must, then prepend bool
            $query = collect(['bool' => $query->toArray()]);
        } elseif ($query->count() == 1 && $found = $query->get('must', false)) {
            // If there is only one boolean section, AND it is must, then prefixing the boolean is removed.
            $query = collect($found);
        }


        return $query;
    }

    /**
     * @param LeafInterface $leaf
     */
    function add(LeafInterface $leaf)
    {
        $this->leaves->push($leaf);
    }

    function attachPoint($prefix = null): string
    {
        return $this->attachPoint;
    }

    /**
     * @param string $attachPoint
     */
    protected function setAttachPoint(string $attachPoint)
    {
        $this->attachPoint = $attachPoint;
    }

    /**
     * @param Filters $filters
     * @return $this
     */
    function setFilters(Filters $filters)
    {
        $this->_queryFilters = $filters;

        return $this;
    }

    protected function hasFilters()
    {
        return ($this->_queryFilters instanceof Filters && $this->_queryFilters->isNotEmpty());
    }

    public function matchAll()
    {
        return ['match_all' => (object) []];
    }

    private function addFilter(Filter $argument)
    {
        if (!$this->hasFilters()) {
            $this->setFilters(\GrizzlyViking\QueryBuilder\Branches\Factories\Filters::create($argument));
        } else {
            $this->_queryFilters->add($argument);
        }
    }

    public function setScripts($script)
    {
        $this->setAttachPoint('query.function_score');

        if ($script instanceof Scripts) {
            $this->scripts = $script;
        } else {
            $this->scripts = \GrizzlyViking\QueryBuilder\Branches\Factories\Scripts::create($script);
        }
    }

    /**
     * @return Scripts
     */
    protected function getScripts()
    {
        return $this->scripts;
    }
}