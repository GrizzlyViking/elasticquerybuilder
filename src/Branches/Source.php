<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/05/2017
 * Time: 11:24
 */

namespace GrizzlyViking\QueryBuilder\Branches;


use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\ManageBranchTrait;
use Illuminate\Support\Collection;

class Source implements BranchInterface
{
    use ManageBranchTrait;
    /** @var  Collection */
    protected $_fields;

    public function __construct(...$fields)
    {
        $this->set($fields);
    }


    /**
     * @param array $value
     * @return $this
     */
    function set($value)
    {
        $this->_fields = collect($value)->flatten();

        return $this;
    }

    /**
     * @return Collection
     */
    function get(): Collection
    {
        return $this->_fields;
    }

    /**
     * @return string
     */
    function getType() : string
    {
        return 'source';
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {
        return $this->get();
    }

    /**
     * @return string
     */
    public function attachPoint(): string
    {
        return '_source';
    }

    /**
     * @param LeafInterface[] ...$leaf
     * @return $this
     */
    function add(LeafInterface ...$leaf)
    {
        return $this;
    }
}