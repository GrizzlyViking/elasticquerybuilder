<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 31/05/2017
 * Time: 17:31
 */

namespace GrizzlyViking\QueryBuilder\Branches;


use GrizzlyViking\QueryBuilder\Leaf\Filter;
use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\Leaf\Query;
use GrizzlyViking\QueryBuilder\ManageBranchTrait;
use GrizzlyViking\QueryBuilder\StaticTrait;
use Illuminate\Support\Collection;

class Filters
{
    use ManageBranchTrait;
    /** @var  Collection */
    protected $_leaves;
    /** @var string */
    protected $attachPoint = 'post_filter';

    public function __construct()
    {
        $this->_leaves = collect();
    }

    public function get() : Collection
    {
        return $this->_leaves;
    }

    /**
     * @return string
     */
    public function attachPoint(): string
    {
        return $this->attachPoint;
    }

    /**
     * @param string $attachPoint
     */
    public function setAttachPoint(string $attachPoint): void
    {
        $this->attachPoint = $attachPoint;
    }

    /**
     * If leaf is a Filter.post_filter or query leaf then it returns a new instance of Filters branch
     *
     * @return Filters
     */
    function getPostFilters()
    {
        return $this->getFilterByAttachPoint('post_filter');
    }

    function has(string $key)
    {
        switch ($key) {
            case 'post_filter':
                return $this->getFilterByAttachPoint('post_filter')->isNotEmpty();
                break;
            case 'filter': //query filter
            case 'query_filter':
                return $this->getFilterByAttachPoint('filter')->isNotEmpty();
            default:
                return $this->_leaves->search(function(Filter $filter) use ($key) {
                    return ($filter instanceof Filter && $filter->getKey() == $key);
                }) !== false;
        }
    }

    /**
     * @param string $key
     * @return $this
     */
    function forget($key)
    {
        $this->_leaves = $this->_leaves->filter(function($filter) use ($key) {

            /** @var Filter $filter */
            return ($filter->getKey() != $key);
        });

        return $this;
    }

    /**
     * if leaf is a Filter and attach point is 'filter' then returns a new instance of Filters branch
     *
     * @return Filters
     */
    function getQueryFilters()
    {
        return $this->getFilterByAttachPoint('filter');
    }

    /**
     * @param string $attach_point
     * @return Filters
     */
    protected function getFilterByAttachPoint($attach_point = 'post_filter')
    {
        $newFilters = new self();
        $this->_leaves->filter(function($leaf) use ($attach_point) {
            if ($leaf instanceof Filter) {
                return ($leaf->getAttachPoint() == $attach_point);
            }

            return $attach_point == 'post_filter';
        })->each(function($filter) use (&$newFilters) {
            $newFilters->add($filter);
        });

        return $newFilters;
    }

    /**
     * @return bool
     */
    private function shouldUseBool()
    {
        return (
            $this->get()->count() > 1 ||
            $this->get()->first()->getBoolean() !== 'must'
        );
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {

        if ($this->shouldUseBool()) {
            return collect([
                'bool' => $this->get()->groupBy(function ($leaf) {

                    return $leaf->getBoolean();
                })->map(function ($grouping) {

                    if ($grouping->count() == 1) {
                        return $grouping->first()->build();
                    }

                    return $grouping->map(function ($filters) {
                        return $filters->build();
                    })->toArray();
                })->toArray()
            ]);
        } else {
            return collect($this->get()->first()->build());
        }

    }

    /**
     * @param LeafInterface[] ...$leaves
     * @return $this
     */
    function add(LeafInterface ...$leaves)
    {
        $this->set($leaves);

        return $this;
    }

    function set($leaves)
    {
        $this->_leaves = $this->_leaves->merge($leaves);

        return $this;
    }
}