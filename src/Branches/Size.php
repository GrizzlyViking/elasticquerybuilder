<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:38
 */

namespace GrizzlyViking\QueryBuilder\Branches;


use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\ManageBranchTrait;
use Illuminate\Support\Collection;

class Size implements BranchInterface
{
    use ManageBranchTrait;
    /** @var Collection  */
    protected $_branch;

    public function __construct(...$leaves)
    {
        $this->_branch = collect();
        $this->set($leaves);
    }


    function get(): Collection
    {
        return $this->_branch;
    }

    function build(): Collection
    {
        return $this->get();
    }

    function add(LeafInterface ...$leaves)
    {
        $this->set($leaves);

        return $this;
    }

    function attachPoint(): string
    {
        return 'size';
    }

    function set($leaves = null)
    {
        if (!$leaves) return $this;

        // if its a integer
        if ($integer = filter_var($leaves, FILTER_VALIDATE_INT)) {
            $this->_branch->put('size', $integer);

            return $this;
        }

        // if its an array
        if (is_array($leaves)) {
            if (count($leaves) == 1) $size = reset($leaves);
            if (count($leaves) == 2) list ($size, $from) = $leaves;

            if (is_array($size)) {
                if (isset($size['size']) && $size_int = filter_var($size['size'], FILTER_VALIDATE_INT)) {
                    $this->_branch->put('size', $size_int);
                }

                if (isset($size['from']) && filter_var($size['from'], FILTER_VALIDATE_INT)) {
                    $this->_branch->put('from', $from);
                }
            } else {
                if ($size = filter_var($size, FILTER_VALIDATE_INT)) {
                    $this->_branch->put('size', $size);
                }
                if ($from = filter_var($from, FILTER_VALIDATE_INT)) {
                    $this->_branch->put('from', $from);
                }
            }
        }

        return $this;
    }
}