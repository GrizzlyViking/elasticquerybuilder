<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 14/06/2017
 * Time: 13:26
 */

namespace GrizzlyViking\QueryBuilder\Branches;

use GrizzlyViking\QueryBuilder\BespokeValidationTrait;
use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\Leaf\Suggestion;
use GrizzlyViking\QueryBuilder\ManageBranchTrait;
use Illuminate\Support\Collection;

class Suggestions implements BranchInterface
{
    use BespokeValidationTrait, ManageBranchTrait;
    /** @var Collection */
    protected $_leaves;

    public function __construct(...$arguments)
    {
        $this->_leaves = collect();
        if ($arguments) {
            $this->set($arguments);
        }
    }


    function get(): Collection
    {
        return $this->_leaves;
    }

    function build(): Collection
    {
        return $this->get()->flatMap(function ($leaf) {
            /** @var Suggestion $leaf */
            return [$leaf->getTitle() => $leaf->build()];
        });
    }

    function add(LeafInterface...$leaves)
    {
        if ($this->areTheyAll($leaves, 'is_string')) {
            $this->_leaves->push($leaves);

            return $this;
        }

        foreach ($leaves as $leaf) {
            if ($leaf instanceof Suggestion) {
                $this->_leaves->push($leaf);
            } else {
                $this->_leaves->push(Suggestion::create($leaf));
            }
        }

        return $this;
    }

    function attachPoint(): string
    {
        return 'suggest';
    }

    /**
     * @param array $leaves
     * @return $this
     */
    function set($leaves)
    {
        if ($this->areTheyAll(array_filter($leaves), 'is_string')) {
            $this->_leaves->push(\GrizzlyViking\QueryBuilder\Leaf\Factories\Suggestion::create()->set($leaves));
        } else {
            foreach ($leaves as $leaf) {
                if (!$leaf instanceof Suggestion) {
                    $leaf = \GrizzlyViking\QueryBuilder\Leaf\Factories\Suggestion::create($leaf);
                }

                $this->_leaves->push($leaf);
            }
        }

        return $this;
    }

}