<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:46
 */

namespace GrizzlyViking\QueryBuilder\Branches;


abstract class BranchAbstract
{
    protected $_key;
    protected $_value;
    protected $_type = 'term';
    protected $_allowedTypes = ['term', 'match', 'match_phrase', 'multi_match', 'must_not', 'must', 'terms'];

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        return collect([$this->getKey() => $this->getValue()]);
    }

    /**
     * @param mixed $type
     */
    protected function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return mixed
     */
    public function getType(): string
    {
        return $this->_type;
    }

    /**
     * @param mixed $key
     */
    protected function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * @return mixed
     */
    protected function getKey()
    {
        return $this->_key;
    }

    /**
     * @param mixed $value
     */
    protected function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * @return mixed
     */
    protected function getValue()
    {
        return $this->_value;
    }
}