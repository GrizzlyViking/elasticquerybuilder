<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:34
 */

namespace GrizzlyViking\QueryBuilder\Branches;


use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\Leaf\Script;
use Illuminate\Support\Collection;

class Scripts implements BranchInterface
{
    /** @var string */
    protected $score_mode = false;
    /** @var string */
    protected $boost_mode = false;

    /**
     * @var Collection
     */
    protected $_scripts;

    public function __construct($arguments = null)
    {
        $this->_scripts = collect();
        $this->set($arguments);
    }

    /**
     * @return Collection
     */
    function get(): Collection
    {
        return $this->_scripts;
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {
        return $this->get()->map(function($leaf) {
            /** @var Script $leaf */
            return $leaf->build();
        });
    }

    /**
     * @param LeafInterface[] ...$leaves
     * @return $this
     */
    function add(LeafInterface ...$leaves)
    {
        $this->_scripts = $this->_scripts->merge($leaves);

        return $this;
    }

    /**
     * @return string
     */
    function attachPoint(): string
    {
        return 'script';
    }

    /**
     * @param array $leaves
     * @return $this
     */
    function set($leaves)
    {
        if ($leaves && is_array($leaves)) {
            if ($score_mode = array_get($leaves, 'score_mode', false)) {
                $this->setScoreMode($score_mode);
            }

            if ($boost_mode = array_get($leaves, 'boost_mode', false)) {
                $this->setBoostMode($boost_mode);
            }

            if ($script = array_get($leaves, 'script', false)) {
                if ($script instanceof Script) {
                    $this->add($script);
                } elseif(is_string($script) || is_array($script)){
                    $this->add(\GrizzlyViking\QueryBuilder\Leaf\Factories\Script::create($script));
                }
            }
        } elseif ($leaves && is_string($leaves)) {
            $this->add(\GrizzlyViking\QueryBuilder\Leaf\Factories\Script::create($leaves));
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isNotEmpty()
    {
        return $this->_scripts->isNotEmpty();
    }

    /**
     * @return bool
     */
    public function hasScoreMode()
    {
        return ($this->getScoreMode() || $this->getBoostMode());
    }

    /**
     * @return string
     */
    public function getScoreMode(): string
    {
        return $this->score_mode;
    }

    /**
     * @param string $score_mode
     * @return Scripts
     */
    public function setScoreMode(string $score_mode)
    {
        $this->score_mode = $score_mode;

        return $this;
    }

    /**
     * @return string
     */
    public function getBoostMode(): string
    {
        return $this->boost_mode;
    }

    /**
     * @param string $boost_mode
     * @return Scripts
     */
    public function setBoostMode(string $boost_mode)
    {
        $this->boost_mode = $boost_mode;

        return $this;
    }


}
