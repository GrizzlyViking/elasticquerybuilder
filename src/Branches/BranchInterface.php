<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:36
 */

namespace GrizzlyViking\QueryBuilder\Branches;

use Illuminate\Support\Collection;

interface BranchInterface
{
    /**
     * @return Collection
     */
    function get(): Collection;

    /**
     * @return Collection
     */
    function build(): Collection;

    /**
     * @return string
     */
    function attachPoint(): string;

}