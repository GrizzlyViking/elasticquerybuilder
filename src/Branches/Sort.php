<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/05/2017
 * Time: 14:28
 */

namespace GrizzlyViking\QueryBuilder\Branches;


use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use GrizzlyViking\QueryBuilder\ManageBranchTrait;
use GrizzlyViking\QueryBuilder\StaticTrait;
use Illuminate\Support\Collection;

class Sort implements BranchInterface
{
    use ManageBranchTrait;
    const DEFAULT_DIRECTION = 'asc';
    protected $_sort;

    public function __construct()
    {
        $this->_sort = collect();
    }

    /**
     * @param string|array|Collection $argument
     * @return $this
     */
    function set($argument)
    {

        if (is_string($argument)) {

            $this->_sort->put($argument, self::DEFAULT_DIRECTION);
        } else {
            $this->_sort = $this->_sort->merge($argument);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    function get(): Collection
    {
        return $this->_sort;
    }

    function getType(): string
    {
        return 'Sort';
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {
        return $this->get();
    }

    function attachPoint(): string
    {
        return 'sort';
    }

    function add(LeafInterface ...$leaf)
    {
        $this->set($leaf);
    }


}