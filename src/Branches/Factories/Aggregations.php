<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 10/07/2017
 * Time: 11:20
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;

use GrizzlyViking\QueryBuilder\Branches\Aggregations as AggBranch;
use GrizzlyViking\QueryBuilder\Leaf\Factories\Aggregation as AggregationFactory;
use GrizzlyViking\QueryBuilder\Leaf\Aggregation;

class Aggregations
{
    /**
     * @param array ...$arguments
     * @return AggBranch
     */
    public static function create(...$arguments)
    {
        $aggs = new AggBranch();
        collect($arguments)->each(function($argument) use ($aggs) {
            if ( ! $argument instanceof Aggregation) {
                $argument = AggregationFactory::create($argument);
            }

            $aggs->add($argument);
        });

        return $aggs;
    }
}