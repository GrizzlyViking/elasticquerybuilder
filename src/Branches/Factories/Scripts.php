<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 11/07/2017
 * Time: 13:36
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


use GrizzlyViking\QueryBuilder\Leaf\Script;

class Scripts
{
    public static function create(...$arguments)
    {
        return new \GrizzlyViking\QueryBuilder\Branches\Scripts(...$arguments);
    }
}