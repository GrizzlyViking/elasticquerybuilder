<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 10/07/2017
 * Time: 09:21
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;

use GrizzlyViking\QueryBuilder\Branches\Filters as FilterBranch;
use GrizzlyViking\QueryBuilder\Leaf\Filter as FilterLeaf;

class Filters
{
    /**
     * @param array ...$arguments
     * @return FilterBranch
     */
    public static function create(...$arguments)
    {
        $filters = new FilterBranch();

        foreach ($arguments as $argument) {
            if ( ! $argument instanceof FilterLeaf) {
                $argument = new FilterLeaf($argument);
            }

            $filters->add($argument);
        }

        return $filters;
    }
}