<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 12/07/2017
 * Time: 08:58
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


use GrizzlyViking\QueryBuilder\Branches\Size;

class Pagination
{
    public static function create($arguments)
    {
        return (new Size())->set($arguments);
    }
}