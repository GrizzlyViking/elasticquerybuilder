<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 12/07/2017
 * Time: 08:50
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


class Source
{
    /**
     * @param array ...$arguments
     * @return \GrizzlyViking\QueryBuilder\Branches\Source;
     */
    public static function create(...$arguments)
    {
        return (new \GrizzlyViking\QueryBuilder\Branches\Source())->set($arguments);
    }
}