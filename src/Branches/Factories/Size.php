<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 12/07/2017
 * Time: 08:50
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


class Size
{
    /**
     * @param array ...$arguments
     * @return \GrizzlyViking\QueryBuilder\Branches\Size;
     */
    public static function create(...$arguments)
    {
        return new \GrizzlyViking\QueryBuilder\Branches\Size(...$arguments);
    }
}