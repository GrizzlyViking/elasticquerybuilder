<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 11/07/2017
 * Time: 14:36
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


class Sort
{
    /**
     * @param array ...$argument
     * @return \GrizzlyViking\QueryBuilder\Branches\Sort;
     */
    public static function create(...$argument)
    {
        $sort = new \GrizzlyViking\QueryBuilder\Branches\Sort();

        foreach ($argument as $item) {
            $sort->set($item);
        }

        return $sort;
    }
}