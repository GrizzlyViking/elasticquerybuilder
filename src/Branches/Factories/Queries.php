<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 11/07/2017
 * Time: 13:08
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


use GrizzlyViking\QueryBuilder\Leaf\Factories\Query as QueryFactory;
use GrizzlyViking\QueryBuilder\Leaf\Query;
use GrizzlyViking\QueryBuilder\Leaf\QueryInterface;

class Queries
{
    public static function create(...$arguments)
    {
        return new \GrizzlyViking\QueryBuilder\Branches\Queries(...$arguments);
    }
}