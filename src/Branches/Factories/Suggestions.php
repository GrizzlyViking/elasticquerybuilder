<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 11/07/2017
 * Time: 13:52
 */

namespace GrizzlyViking\QueryBuilder\Branches\Factories;


use GrizzlyViking\QueryBuilder\BespokeValidationTrait;
use GrizzlyViking\QueryBuilder\Leaf\Suggestion;

class Suggestions
{
    use BespokeValidationTrait;
    /**
     * @param array ...$arguments
     * @return \GrizzlyViking\QueryBuilder\Branches\Suggestions
     */
    public static function create(...$arguments)
    {

        $suggestions = new \GrizzlyViking\QueryBuilder\Branches\Suggestions();

        $suggestions->set($arguments);

        return $suggestions;
    }
}