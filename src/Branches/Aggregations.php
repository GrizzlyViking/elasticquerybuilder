<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/05/2017
 * Time: 14:27
 */

namespace GrizzlyViking\QueryBuilder\Branches;

use GrizzlyViking\QueryBuilder\Leaf\Aggregation;
use GrizzlyViking\QueryBuilder\Leaf\LeafInterface;
use Illuminate\Support\Collection;

class Aggregations extends BranchAbstract
{

    /** @var  Collection */
    protected $_leaves;
    /** @var  Filters */
    protected $_filters;

    public function __construct()
    {
        $this->_leaves = collect();
    }

    /**
     * @param LeafInterface $leaf
     * @return $this
     */
    function add($leaf)
    {
        /** @var Aggregation $leaf */
        $this->_leaves->put($leaf->getTitle(), $leaf);

        return $this;
    }

    /**
     * @return string
     */
    function attachPoint(): string
    {
        return 'aggregations';
    }

    /**
     * @return bool
     */
    public function isNotEmpty()
    {
        return $this->_leaves->isNotEmpty();
    }

    public function setFilters(Filters $filters)
    {
        $this->_filters = $filters;

        return $this;
    }

    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {
        return $this->_leaves->map(function ($aggregation) {
            if (!$aggregation instanceof Aggregation) {
                return false;
            }

            if ($this->getFilters() !== null && $this->getFilters()->getPostFilters() !== null) {

                return $aggregation->setFilters(
                    $this->getFilters()
                        ->getPostFilters()
                        ->forget(preg_replace('/\..*$/', '', $aggregation->getField()))
                )->build();
            }

            return $aggregation->build();
        })->filter();
    }

    /**
     * @return Collection
     */
    public function getLeaves()
    {
        return $this->_leaves;
    }

    public function getLeaf($leafKey)
    {
        return $this->_leaves->get($leafKey, false);
    }
}