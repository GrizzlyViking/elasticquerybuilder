<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 25/05/2018
 * Time: 15:22
 */

namespace GrizzlyViking\QueryBuilder;


interface ResponseInterface
{
    public function all();
}