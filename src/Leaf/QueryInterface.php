<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/08/2017
 * Time: 10:24
 */

namespace GrizzlyViking\QueryBuilder\Leaf;


interface QueryInterface
{
    function getBoolean();
}