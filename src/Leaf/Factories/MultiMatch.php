<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 12/07/2017
 * Time: 08:45
 */

namespace GrizzlyViking\QueryBuilder\Leaf\Factories;


class MultiMatch
{
    /**
     * @param string $searchTerm
     * @return \GrizzlyViking\QueryBuilder\Leaf\MultiMatch
     */
    public static function create($searchTerm)
    {
        return new \GrizzlyViking\QueryBuilder\Leaf\MultiMatch($searchTerm);
    }
}