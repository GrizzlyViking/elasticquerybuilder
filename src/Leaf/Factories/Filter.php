<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 10/07/2017
 * Time: 09:37
 */

namespace GrizzlyViking\QueryBuilder\Leaf\Factories;

class Filter extends Query
{
    /**
     * @param array ...$arguments
     * @return \GrizzlyViking\QueryBuilder\Leaf\Filter
     */
    public static function create(...$arguments)
    {
        return new \GrizzlyViking\QueryBuilder\Leaf\Filter(...$arguments);
    }
}