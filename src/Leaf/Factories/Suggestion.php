<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 11/07/2017
 * Time: 13:49
 */

namespace GrizzlyViking\QueryBuilder\Leaf\Factories;


class Suggestion
{
    /**
     * @param $argument
     * @return \GrizzlyViking\QueryBuilder\Leaf\Suggestion
     */
    public static function create(...$argument)
    {
        return (new \GrizzlyViking\QueryBuilder\Leaf\Suggestion())->set($argument);
    }
}