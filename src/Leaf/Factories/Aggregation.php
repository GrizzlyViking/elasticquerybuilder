<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 10/07/2017
 * Time: 11:13
 */

namespace GrizzlyViking\QueryBuilder\Leaf\Factories;

use GrizzlyViking\QueryBuilder\Leaf\Aggregation as AggLeaf;

class Aggregation
{
    public static function create($arguments)
    {
        return new AggLeaf($arguments);
    }
}