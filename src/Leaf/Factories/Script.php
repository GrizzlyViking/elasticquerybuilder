<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 11/07/2017
 * Time: 13:41
 */

namespace GrizzlyViking\QueryBuilder\Leaf\Factories;


class Script
{
    public static function create($argument)
    {
        return new \GrizzlyViking\QueryBuilder\Leaf\Script($argument);
    }
}