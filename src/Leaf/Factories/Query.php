<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 10/07/2017
 * Time: 12:03
 */

namespace GrizzlyViking\QueryBuilder\Leaf\Factories;

use GrizzlyViking\QueryBuilder\Leaf\Query as Leaf;

class Query
{
    public static function create(...$arguments)
    {
        return new Leaf(...$arguments);
    }
}