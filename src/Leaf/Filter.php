<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/08/2017
 * Time: 10:27
 */

namespace GrizzlyViking\QueryBuilder\Leaf;


class Filter extends Query
{
    protected $_attachPoint = 'post_filter';

    /**
     * @return string
     */
    public function getAttachPoint()
    {
        if ($this->_attachPoint === null) {
            $this->setAttachPoint('post_filter');
        }

        return $this->_attachPoint;
    }

    /**
     * @param string $attachPoint
     * @return $this
     */
    public function setAttachPoint($attachPoint)
    {
        $this->_attachPoint = $attachPoint;

        return $this;
    }

    public function postFilter()
    {
        return $this->setAttachPoint('post_filter');
    }

    public function queryFilter()
    {
        return $this->setAttachPoint('filter');
    }
}