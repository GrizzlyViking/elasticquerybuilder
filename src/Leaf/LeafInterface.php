<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:41
 */

namespace GrizzlyViking\QueryBuilder\Leaf;


interface LeafInterface
{
    function build();
}