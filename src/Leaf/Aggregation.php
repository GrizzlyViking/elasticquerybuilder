<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 01/06/2017
 * Time: 12:51
 */

namespace GrizzlyViking\QueryBuilder\Leaf;

use GrizzlyViking\QueryBuilder\Branches\Filters;
use Illuminate\Support\Collection;

class Aggregation implements LeafInterface
{
    protected $field;
    protected $title;
    protected $size;
    protected $order;
    protected $type = 'terms';
    /** @var  Collection */
    protected $ranges;
    protected $filters;
    /** @var Collection */
    protected $bucket_filters;
    /** @var \Closure */
    protected $callback;
    /** @var array|bool */
    protected $sampler = false;

    public function __construct($argument = null)
    {
        if ($argument) $this->set($argument);
    }

    public function set($argument)
    {
        collect($argument)->each(function($value, $key){
            switch (strtolower($key)) {
                case 'order':
                case 'sort':
                    $this->setOrder($value);
                    break;
                case 'field':
                    $this->setField($value);
                    break;
                case 'size':
                    $this->setSize($value);
                    break;
                case 'title':
                    $this->setTitle($value);
                    break;
                case 'ranges':
                    $this->setRanges($value);
                    break;
                case 'callback':
                    $this->setCallback($value);
                    break;
                case 'sampler':
                    $this->setSampler($value);
                    break;
                case 'query':
                    $this->setBucketFilters($value());
                    break;
                case 'filters':
                    $this->setBucketFilters($value());
                    break;
            }
        });
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param Filters $filters
     * @return $this
     */
    public function setFilters($filters)
    {
        if($filters) {
            $filters->forget($this->field);
            $this->filters = $filters;
        }

        return $this;
    }

    /**
     * @return Filters
     */
    protected function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        if ($this->getRanges() !== null) {
            return 'range';
        } elseif ($this->getBucketFilters()->isNotEmpty()) {
            return 'filters';
        }

        return $this->type;
    }

    /**
     * @param array $ranges
     */
    public function setRanges(array $ranges)
    {
        $this->ranges = collect($ranges);
    }

    /**
     * @return Collection|null
     */
    public function getRanges()
    {
        return $this->ranges;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string|callable $field
     */
    public function setField($field)
    {
        if (is_callable($field)) {
            $this->field = $field();
        } else {

            $this->field = $field;
        }
    }

    /**
     * @param string|array $order
     * @return $this
     */
    public function setOrder($order)
    {
        if (is_string($order)) {
            $this->order = [$order => ['_count' => 'desc']];

            return $this;
        }

        $this->order = collect($order)->map(function($value){

            if (is_string($value) && in_array(strtolower($value), ['asc', 'desc'])) {
                return ['_count' => $value];
            };

            return $value;
        })->toArray();

        return $this;
    }

    /**
     * @return string|array
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param integer $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        if ( ! $this->title) {
            return $this->getField();
        }
        return $this->title;
    }

    /**
     * @return Collection
     */
    function build(): Collection
    {
        if ($this->getBucketFilters()->isNotEmpty()) {
            $aggregation = collect([$this->getType() => $this->getBucketFilters()->toArray()]);
        } else {
            $aggregation = collect([
                $this->getType() => collect([
                    'field'  => $this->getField(),
                    'size'   => $this->getSize(),
                    'keyed'  => ($this->getRanges() instanceof Collection && isset($this->getRanges()->first()['key'])) ?: null,
                    'order'  => $this->getOrder(),
                    'ranges' => $this->getRanges()
                ])->filter()
            ]);
        }

        if (($this->getFilters() && $this->getFilters()->isNotEmpty())) {
            $aggregation = collect([
                'filter' => $this->getFilters()->build()->toArray(),
                'aggregations' => [$this->getTitle() => $aggregation->toArray()]
            ]);
        }

        if ($this->getSampler()) {
            $aggregation = collect([
                'sampler' => $this->getSampler(),
                'aggregations' => [$this->getTitle() => $aggregation->toArray()]
            ]);
        }

        return $aggregation;
    }

    /**
     * @param $callback
     */
    protected function setCallback($callback)
    {
        if (is_callable($callback)) {
            $this->callback = $callback;
        }
    }

    /**
     * @return \Closure
     */
    public function getCallback()
    {
        return $this->callback;
    }

    private function setSampler($value)
    {
        $this->sampler = $value;
    }

    /**
     * @return array|bool
     */
    private function getSampler()
    {
        return $this->sampler;
    }

    /**
     * @return Collection
     */
    public function getBucketFilters(): Collection
    {
        if ( ! $this->bucket_filters) return collect();
        return $this->bucket_filters;
    }

    /**
     * @param array $bucket_filters
     */
    public function setBucketFilters(array $bucket_filters)
    {
        $this->bucket_filters = collect($bucket_filters);
    }
}