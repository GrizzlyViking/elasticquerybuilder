<?php

namespace GrizzlyViking\QueryBuilder\Leaf;

use GrizzlyViking\QueryBuilder\Branches\Factories\Queries;
use Illuminate\Support\Collection;

class Query implements LeafInterface, QueryInterface
{
    protected $_key;
    protected $_value;
    protected $_type;
    /** @var bool|string */
    protected $_boolean      = false;
    protected $_allowedTypes = ['term', 'match', 'match_phrase', 'multi_match', 'terms', 'range'];
    protected static $_allowedBooleans = ['should', 'must', 'must_not'];

    public function __construct(...$arguments)
    {
        $this->set(...$arguments);
    }

    /**
     * @param string $boolean
     * @return boolean
     */
    public function setBoolean($boolean = 'must')
    {
        if (is_string($boolean) && in_array($boolean, self::$_allowedBooleans, true)) {

            $this->_boolean = $boolean;

            return true;
        }

        return false;
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        if ($this->getType()) {
            return collect([$this->getType() => [$this->getKey() => $this->getValue()]]);
        }

        return collect([$this->getKey() => $this->getValue()]);
    }

    /**
     * @return string|bool
     */
    public function getBoolean()
    {
        return $this->_boolean;
    }

    /**
     * @param array $arguments
     * @return $this
     */
    public function set(...$arguments)
    {
        foreach ($arguments as $argument) {

            if (is_string($argument) && $this->setBoolean($argument)) continue;

            if ($this->setType($argument)) continue;
            if (is_array($argument) && $this->setType(key($argument))) {
                $argument = array_get($argument, $this->getType());
            }

            if ($argument == 'match_all') {
                $this->setKey('match_all');
                $this->setValue((object)[]);

                return $this;
            }

            if (empty($argument)) {
                return $this;
            }

            if (is_string($argument)) continue;

            if (count($argument) == 1) {
                if (reset($argument) == 'match_all') {
                    $this->setKey('match_all');
                    $this->setValue((object)[]);

                    return $this;
                }

                if (reset($argument) === null) {
                    return $this;
                }
            }

            $this->parseArray($argument);
        }

        return $this;
    }

    public function build()
    {
        if ($this->getValue() instanceof \GrizzlyViking\QueryBuilder\Branches\Queries) {
            return $this->getValue()->build()->toArray();
        }

        if ($this->getKey() == 'match_all') {
            return [$this->getKey() => $this->getValue()];
        }

        return [$this->getType() => [$this->getKey() => $this->getValue()]];
    }

    /**
     * @param $type
     * @return bool
     */
    public function setType($type)
    {
        if (in_array($type, $this->_allowedTypes, true)) {
            $this->_type = $type;

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        if ($this->_key === null && $this->getValue() instanceof \GrizzlyViking\QueryBuilder\Branches\Queries && $this->getValue()->get()->count() >= 2) {

            return $this->getValue()->get()->first()->getKey();
        }

        return $this->_key;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $value = $this->cleanString($value);
        $this->_value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Tries to guess which type it is, based on the payload.
     *
     * @param $payload
     */
    public function guessType($payload)
    {
        if (is_array($payload)) {
            $payload = $this->drilldown($payload);
        }
        if (is_string($payload)) {
            if (strpos($payload, ' ') !== false) {
                $this->setType('match_phrase');
            } else {
                $this->setType('match');
            }
        } elseif (is_string($payload) && filter_var($payload, FILTER_VALIDATE_INT)) {
            $this->setType('term');
        }

        return $payload;
    }

    protected function cleanString($string)
    {
        if (is_array($string)) {
            $cleanedArray = [];
            foreach ($string as $key => $value) {
                $cleanedArray[$key] = $this->cleanString($value);
            }

            return $cleanedArray;
        }

        if (!is_string($string)) {
            return $string;
        }

        if (preg_match('/^!/', $string)) {
            $this->setBoolean('must_not');
        }

        return preg_replace('/[^a-zA-Z0-9\.\'\s\-\(\)]+/', '', $string);
    }

    private function drilldown($array)
    {
        if (!is_array($array)) {
            return $array;
        }

        if (count($array) > 1) {
            return $array;
        }

        if (in_array(key($array), ['gt', 'gte', 'lte', 'lt'], true)) {
            return $array;
        }
        return $this->drilldown(array_get($array, key($array)));
    }

    /**
     * @param array $argument
     */
    private function parseArray(array $argument)
    {
        if (in_array($boolean = key($argument), self::$_allowedBooleans) && is_array($values = array_get($argument, $boolean))) {

            if (count($values) > 1) {
                $branch = Queries::create(...collect($values)->map(function ($value) use ($boolean) {
                    $leaf = new self($value);
                    $leaf->setBoolean($boolean);

                    return $leaf;
                })->toArray());

                $this->setValue($branch);

                if (!$this->getBoolean()) {
                    $this->setBoolean('must');
                }

                return $this;
            }

            if($this->setBoolean($boolean)) {
                $argument = array_get($argument, $boolean);

                if (($key = filter_var(key($argument), FILTER_VALIDATE_INT)) !== false) {
                    $argument = $argument[$key];
                }
            }
        }

        if (!$this->getBoolean()) {
            if($this->setBoolean(key($argument))) {
                $argument = array_get($argument, $this->getKey());
            } else {
                $this->setBoolean('must');
            }

        }

        if (!$this->getType()) {
            if ($this->setType(key($argument))) {
                $argument = array_get($argument, $this->getType());
            } else {
                $this->guessType($argument);
            }
        }

        if (is_array($argument)) {
            $this->setKey(key($argument));
        }

        if ($this->getType() === 'terms') {
            $this->setValue(array_get($argument, $this->getKey()));
        } else {
            $this->setValue($this->drilldown($argument));
        }

        return $this;
    }
}