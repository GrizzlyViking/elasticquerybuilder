<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/05/2017
 * Time: 14:28
 */

namespace GrizzlyViking\QueryBuilder\Leaf;

use Illuminate\Support\Collection;

class Script implements LeafInterface
{
    /** @var  Collection */
    protected $_script;

    public function __construct(...$script)
    {
        $this->set(...$script);
    }

    /**
     * @param mixed $script
     * @return $this
     */
    public function set($script)
    {
        $this->_script = collect(['lang' => 'painless']);

        if ($inline = array_get($script, 'script', false)) {

            $this->_script->put('inline', $inline);
        }

        if ($score_mode = array_get($script,'score_mode', false)) {
            $this->_script->put('score_mode', $score_mode);
        }

        if ($boost_mode = array_get($script,'boost_mode', false)) {
            $this->_script->put('boost_mode', $boost_mode);
        }

        if (is_string($script)) {
            $this->_script->put('inline', $script);
        }

        return $this;
    }

    public function __get($name)
    {
        if (!is_string($name)) {
            return '';
        }

        return $this->_script->get($name, false);
    }

    /**
     * @return array
     */
    function build()
    {
        return $this->_script->except(['boost_mode', 'score_mode'])->toArray();
    }
}