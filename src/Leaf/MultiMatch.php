<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/05/2017
 * Time: 13:35
 */

namespace GrizzlyViking\QueryBuilder\Leaf;

class MultiMatch implements LeafInterface, QueryInterface
{
    protected $_searchTerm;
    protected $_multiMatchType = 'cross_fields';
    protected $_boolean = 'must';
    protected $_operator = 'and';
    /** @var string|bool  */
    protected $_analyzer = false;
    protected $_fields = [
        "title",
        "contributors",
        "series",
        "publisher"
    ];

    function set($arguments)
    {
        $this->setSearchTerm($arguments);
    }

    function getBoolean()
    {
        return $this->_boolean;
    }

    /**
     * @param string $boolean
     */
    public function setBoolean(string $boolean)
    {
        $this->_boolean = $boolean;
    }

    /**
     * MultiMatch constructor.
     * @param string $searchTerm
     */
    public function __construct($searchTerm = null)
    {
        $this->setSearchTerm($searchTerm);
    }

    public static function create($searchTerm)
    {
        return new MultiMatch($searchTerm);
    }

    public function build()
    {
        $multi = [
            'multi_match' => [
                'query'    => $this->getSearchTerm(),
                'type'     => $this->_multiMatchType,
                'operator' => $this->_operator,
                'fields'   => $this->_fields
            ]
        ];

        if ($this->_analyzer) {
            array_set($multi, 'multi_match.analyzer', $this->_analyzer);
        }

        return $multi;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->_fields = $fields;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return config();
    }

    /**
     * @param string $multiMatchType
     */
    public function setMultiMatchType($multiMatchType)
    {
        $this->_multiMatchType = $multiMatchType;
    }

    /**
     * @param string $searchTerm
     */
    public function setSearchTerm(string $searchTerm)
    {
        $this->_searchTerm = $searchTerm;
    }

    /**
     * @return string|null
     */
    public function getSearchTerm()
    {
        return $this->_searchTerm;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {

        $this->_operator = (in_array(strtolower($operator), ['and', 'or']) ? strtolower($operator) : 'and');
    }

    public function setAnalyzer($analyzer)
    {
        $this->_analyzer = $analyzer;
    }
}