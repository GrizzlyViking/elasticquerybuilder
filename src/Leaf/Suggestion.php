<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 14/06/2017
 * Time: 12:42
 */

namespace GrizzlyViking\QueryBuilder\Leaf;


class Suggestion implements LeafInterface
{
    /** @var  string */
    protected $field;
    /** @var  string */
    protected $searchTerm;
    /** @var string */
    protected $title;

    /**
     * @param string $searchTerm
     * @return $this;
     */
    public function setSearchTerm($searchTerm): Suggestion
    {
        if ( ! is_string($searchTerm)) {
            throw new \InvalidArgumentException('search term should be a string. ' .  var_export($searchTerm, 1) . ' given.');
        }

        $this->searchTerm = $searchTerm;

        return $this;
    }

    /**
     * @return string
     */
    protected function getSearchTerm(): string
    {
        return $this->searchTerm;
    }

    /**
     * @param string $field
     * @return $this
     */
    public function setField(string $field) : Suggestion
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    protected function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title) : Suggestion
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        if (!$this->title && $this->field) {
            return $this->field;
        }
        return $this->title;
    }

    /**
     * @param array $arguments either array with [title, field, searchterm], or searchterm, field, title(optional)
     * @return $this
     */
    function set($arguments) : Suggestion
    {
        $arguments = collect($arguments)->filter();

        if (is_array($arguments->first())) {

            collect($arguments->first())->each(function($value, $key) {
                switch (strtolower($key)) {
                    case 'title':
                        $this->setTitle($value);
                        break;
                    case 'field':
                        $this->setField($value);
                        break;
                    case 'searchterm':
                        $this->setSearchTerm($value);
                        break;
                }
            });

            return $this;
        }

        if ($arguments->count() >= 2) {
            $this->setSearchTerm($arguments->first());
            $this->setField($arguments->values()->get(1));
        }

        if($arguments->count() == 3) {
            $this->setTitle($arguments->values()->get(2));
        }

        return $this;
    }

    function build()
    {
        return [
            'text' => $this->getSearchTerm(),
            'term' => [
                'field' => $this->getField()
            ]
        ];
    }
}