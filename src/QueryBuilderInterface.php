<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:27
 */

namespace GrizzlyViking\QueryBuilder;

interface QueryBuilderInterface
{
    public function build();
    public function getQuery();
    public function reset();
}
