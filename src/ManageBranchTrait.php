<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2017
 * Time: 15:33
 */

namespace GrizzlyViking\QueryBuilder;


trait ManageBranchTrait
{
    /**
     * @return bool
     */
    public function isNotEmpty()
    {
        return $this->get()->count() > 0;
    }

    public function isEmpty()
    {
        return $this->get()->count() == 0;
    }
}